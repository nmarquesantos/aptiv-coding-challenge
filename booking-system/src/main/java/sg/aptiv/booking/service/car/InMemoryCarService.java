package sg.aptiv.booking.service.car;

import org.springframework.stereotype.Service;
import sg.aptiv.booking.model.Car;
import sg.aptiv.booking.model.Location;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static java.util.function.Predicate.not;

@Service
public class InMemoryCarService implements CarService {

    private Map<Integer, Car> cars;

    public InMemoryCarService() {
        cars = new HashMap<>();
    }

    @Override
    public Car get(int id) {
        return cars.get(id);
    }

    @Override
    public List<Car> getBookedCars() {

        return new ArrayList<>(cars.values()).stream()
                .filter(Car::isBooked)
                .collect(Collectors.toList());
    }

    @Override
    public List<Car> getAvailableCars() {

        return new ArrayList<>(cars.values()).stream()
                .filter(not(Car::isBooked))
                .collect(Collectors.toList());
    }

    @Override
    public void save(Car car) {
        cars.put(car.getId(), car);
    }

    @Override
    public void bookCar(int carId) {
        updateCarBookingStatus(carId, true);
    }

    @Override
    public void freeUpCar(int carId) {
        updateCarBookingStatus(carId, false);
    }

    @Override
    public void updateLocation(int carId, Location newLocation) {
        var car = cars.get(carId);
        car.setCurrentLocation(newLocation);
        cars.put(carId, car);
    }

    @Override
    public void reset() {
        var startingLocation = new Location(0, 0);
        cars.keySet().forEach(carId -> cars.put(carId, new Car(carId, startingLocation)));
    }

    private void updateCarBookingStatus(int carId, boolean isBooked) {
        var car = cars.get(carId);
        car.setBooked(isBooked);
        cars.put(carId, car);
    }

}
