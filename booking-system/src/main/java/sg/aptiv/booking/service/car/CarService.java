package sg.aptiv.booking.service.car;

import sg.aptiv.booking.model.Car;
import sg.aptiv.booking.model.Location;

import java.util.List;

/***
 *
 * CarService provides all required interactions when storing, accessing or modifying Car data.
 *
 * @author Nuno Santos
 * */
public interface CarService {

    Car get(int id);

    List<Car> getBookedCars();

    List<Car> getAvailableCars();

    void save(Car car);

    void bookCar(int carId);

    void freeUpCar(int carId);

    void updateLocation(int carId, Location newLocation);

    void reset();
}
