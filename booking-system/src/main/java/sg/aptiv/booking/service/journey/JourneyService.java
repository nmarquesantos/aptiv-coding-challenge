package sg.aptiv.booking.service.journey;

import sg.aptiv.booking.model.Journey;

import java.util.List;

/**
 * JourneyService provides methods to access, store, and modify journeys.
 *
 * It is also responsible for managing the on-going state of journeys.
 *
 * @author Nuno Santos
 */
public interface JourneyService {

    void addJourney(Journey journey);

    void advanceJourneys(int timeUnit);

    List<Journey> getOnGoingJourneys();

    List<Journey> getFinishedJourneys();

    void clearOnGoingJourneys();

    void clearFinishedJourneys();
}
