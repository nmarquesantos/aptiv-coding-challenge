package sg.aptiv.booking.service.journey;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import sg.aptiv.booking.model.Journey;
import sg.aptiv.booking.service.car.CarService;

import java.util.LinkedList;
import java.util.List;

@Service
public class InMemoryJourneyService implements JourneyService {

    private CarService carRepository;

    private List<Journey> ongoingJourneys;

    private List<Journey> finishedJourneys;

    @Autowired
    public InMemoryJourneyService(CarService carRepository) {
        this.carRepository = carRepository;
        ongoingJourneys = new LinkedList<>();
        finishedJourneys = new LinkedList<>();
    }

    @Override
    public void addJourney(Journey journey) {
        ongoingJourneys.add(journey);
    }

    @Override
    public void advanceJourneys(int timeUnit) {

        var iterator = ongoingJourneys.iterator();
        var ongoingJourneys = new LinkedList<Journey>();
        while (iterator.hasNext()) {
            var journey = iterator.next();

            journey.setTotalTime(journey.getTotalTime() - timeUnit);
            journey.getLocations().pop();

            if (journey.getTotalTime() == 1) {
                //update car location and availability
                finishedJourneys.add(journey);
                carRepository.freeUpCar(journey.getCarId());
                carRepository.updateLocation(journey.getCarId(), journey.getLocations().pop());

            } else {
                //update car location
                ongoingJourneys.add(journey);
            }
        }
        this.ongoingJourneys = ongoingJourneys;
    }

    @Override
    public List<Journey> getOnGoingJourneys() {
        return ongoingJourneys;
    }

    @Override
    public List<Journey> getFinishedJourneys() {
        return finishedJourneys;
    }

    @Override
    public void clearOnGoingJourneys() {
        ongoingJourneys.clear();
    }

    @Override
    public void clearFinishedJourneys() {
        finishedJourneys.clear();
    }
}
