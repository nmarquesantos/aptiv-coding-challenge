package sg.aptiv.booking.service.journey.search.astar;

import java.util.Objects;

/**
 * Represents a node in the 2D Grid, to be used in the AStar search.
 *
 * @author Nuno Santos
 */
public class Node {
    private int x;

    private int y;

    private double distanceToStart;

    public Node(int x, int y) {
        this.x = x;
        this.y = y;
        this.distanceToStart = 0.0;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public void setDistanceToStart(double distanceToStart) {
        this.distanceToStart = distanceToStart;
    }

    public double getDistanceToStart() {
        return distanceToStart;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Node node = (Node) o;
        return x == node.x &&
                y == node.y;
    }

    @Override
    public int hashCode() {
        return Objects.hash(x, y);
    }
}
