package sg.aptiv.booking.service;

import sg.aptiv.booking.model.Car;
import sg.aptiv.booking.model.Journey;
import sg.aptiv.booking.model.Location;

import java.util.List;

/**
 *
 * BookingService allows the creation of bookings and journeys.
 *
 * @author Nuno Santos
 * */
public interface BookingService {

    /**
     *  Iterates over the list of available cars (if any) and
     *  calculates the shortest journey possible between the two locations.
     *
     * @param availableCars a collection of available cars
     * @param source the location where pick-up should happen
     * @param destination the location where drop-off should happen
     * @return the shortest journey from source to destination, with the closest available car.
     */
    Journey makeBooking(List<Car> availableCars, Location source, Location destination);
}
