package sg.aptiv.booking.service.journey.search.astar;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import sg.aptiv.booking.model.Car;
import sg.aptiv.booking.model.Journey;
import sg.aptiv.booking.model.Location;
import sg.aptiv.booking.service.journey.search.SearchService;

import java.util.LinkedList;
import java.util.stream.Collectors;

@Service
public class AStarSearchService implements SearchService {

    private AStarAlgorithm algorithm;

    @Autowired
    public AStarSearchService(AStarAlgorithm algorithm) {
        this.algorithm = algorithm;
    }

    @Override
    public Journey searchShortestJourney(Car car, Location source, Location destination) {
        var sourceSearchNode = parseLocation(source);
        var carSearchNode = parseLocation(car.getCurrentLocation());
        var destinationSearchNode = parseLocation(destination);

        var pickupJourney = algorithm.search(carSearchNode, sourceSearchNode);
        var dropOffJourney = algorithm.search(sourceSearchNode, destinationSearchNode);

        /**
         * Since the last node of the pick-up journey
         * is the same as the first node of the drop-off journey
         * we should remove one to avoid duplicates/
         * */
        dropOffJourney.removeFirst();
        pickupJourney.addAll(dropOffJourney);

        /**
         * the total time of travel is minus one
         * as we won't travel further than the last node
         * */
        var totalTime = pickupJourney.size() - 1;

        var fullJourney = pickupJourney.stream().
                map(node -> new Location(node.getX(), node.getY()))
                .collect(Collectors.toCollection(LinkedList::new));

        return new Journey(totalTime, car.getId(), fullJourney);
    }

    private Node parseLocation(Location location) {
        return new Node(location.getX(), location.getY());
    }
}
