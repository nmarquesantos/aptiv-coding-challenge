package sg.aptiv.booking.service.journey.search;

import sg.aptiv.booking.model.Car;
import sg.aptiv.booking.model.Journey;
import sg.aptiv.booking.model.Location;

/**
 *
 * SearchService is responsible for providing the shortest journey possible
 * (i.e. lowest time units)
 *
 * @author Nuno Santos
 */
public interface SearchService {

    /**
     *
     * @param car the car that will make the journey
     * @param source the pick-up location
     * @param destination the drop-off location
     * @return the journey the car will make for the customer
     */
    Journey searchShortestJourney(Car car, Location source, Location destination);
}
