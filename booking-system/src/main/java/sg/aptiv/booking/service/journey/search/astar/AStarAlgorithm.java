package sg.aptiv.booking.service.journey.search.astar;

import org.springframework.stereotype.Component;

import java.util.*;

/**
 *
 * An implementation of the A-star path search algorithm
 * as described here: https://en.wikipedia.org/wiki/A*_search_algorithm
 *
 * For simplicity, the size of the grid is define by the following constants:
 * MIN_X_DIMENSION, MAX_X_DIMENSION, MIN_Y_DIMENSION and MAX_Y_DIMENSION.
 *
 * @author Nuno Santos
 */
@Component
public class AStarAlgorithm {

    private static final int MIN_X_DIMENSION = -100;

    private static final int MAX_X_DIMENSION = 100;

    private static final int MIN_Y_DIMENSION = -100;

    private static final int MAX_Y_DIMENSION = 100;

    private Map<Node, Double> knownDistances;

    private Map<Coordinate, Node> grid;

    private Queue<Node> priorityQueue;

    public AStarAlgorithm() {
        grid = new HashMap<>();
        knownDistances = new HashMap<>();

        priorityQueue = new PriorityQueue<>(10, (x, y) -> {
            if(x.getDistanceToStart() < y.getDistanceToStart())  {
                return -1;
            }

            if(x.getDistanceToStart() > y.getDistanceToStart()) {
                return 1;
            }

            return 0;
        });

        reset();
    }

    private void reset() {
        priorityQueue.clear();
        knownDistances.clear();
        grid.clear();

        for (int i = MIN_X_DIMENSION; i < MAX_X_DIMENSION; i++) {
            for (int j = MIN_Y_DIMENSION; j < MAX_Y_DIMENSION; j++) {
                var node = new Node(i, j);
                knownDistances.put(node, Double.MAX_VALUE);
                grid.put(new Coordinate(i, j), node);
            }
        }
    }

    private Set<Node> getNeighbors(Node node) {
        var neighbors = new HashSet<Node>();
        var x = node.getX();
        var y = node.getY();

        if (x < MAX_X_DIMENSION) {
            var coordinate = new Coordinate(x + 1, y);
            neighbors.add(grid.get(coordinate));
        }

        if (x > MIN_X_DIMENSION) {
            var coordinate = new Coordinate(x - 1, y);
            neighbors.add(grid.get(coordinate));
        }

        if (y > MIN_Y_DIMENSION) {
            var coordinate = new Coordinate(x, y - 1);
            neighbors.add(grid.get(coordinate));
        }

        if (y < MAX_Y_DIMENSION) {
            var coordinate = new Coordinate(x, y + 1);
            neighbors.add(grid.get(coordinate));
        }

        return neighbors;
    }

    private Deque<Node> reconstructPath(Node start, Node goal, Map<Node, Node> parentMap) {
        var path = new LinkedList<Node>();
        var currNode = goal;
        while (!currNode.equals(start)) {
            path.addFirst(currNode);
            currNode = parentMap.get(currNode);
        }
        path.addFirst(start);
        return path;
    }

    public Deque<Node> search(Node start, Node destination) {
        reset();

        var path= new HashMap<Node, Node>();
        var visited = new HashSet<Node>();

        start.setDistanceToStart(0.0);
        priorityQueue.add(start);

        while (!priorityQueue.isEmpty()) {
            var current = priorityQueue.remove();

            if (!visited.contains(current)) {
                visited.add(current);

                if (current.equals(destination)) {
                    return reconstructPath(start, destination, path);
                }

                var neighbors = getNeighbors(current);

                for (Node neighbor : neighbors) {
                    if (!visited.contains(neighbor)) {
                        var neighborDistance = 1.0;
                        var predictedDistance = Math.abs(neighbor.getX() - destination.getX()) + Math.abs(neighbor.getY() - destination.getY());
                        var totalDistance = current.getDistanceToStart() + neighborDistance + predictedDistance;

                        if (totalDistance < knownDistances.get(neighbor)) {
                            knownDistances.put(neighbor, totalDistance);
                            neighbor.setDistanceToStart(totalDistance);
                            path.put(neighbor, current);
                            priorityQueue.add(neighbor);
                        }
                    }
                }
            }
        }
        return null;
    }
}
