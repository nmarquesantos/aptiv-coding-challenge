package sg.aptiv.booking.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import sg.aptiv.booking.model.Car;
import sg.aptiv.booking.model.Journey;
import sg.aptiv.booking.model.Location;
import sg.aptiv.booking.service.journey.search.SearchService;

import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;

@Service
public class SimpleBookingService implements BookingService {

    private SearchService journeyService;

    @Autowired
    public SimpleBookingService(SearchService journeyService) {
        this.journeyService = journeyService;
    }

    @Override
    public Journey makeBooking(List<Car> availableCars, Location source, Location destination) {

        if (availableCars == null || availableCars.isEmpty()) {
            throw new IllegalArgumentException("Available cars is empty or null");
        }

        if (source == null) {
            throw new IllegalArgumentException("No source.");
        }

        if (destination == null) {
            throw new IllegalArgumentException("No destination");
        }

        var journeys = new LinkedList<Journey>();
        availableCars.forEach(car -> {
            journeys.add(journeyService.searchShortestJourney(car, source, destination));
        });

        return journeys.stream()
                .min(Comparator.comparing(Journey::getTotalTime))
                .get();
    }
}
