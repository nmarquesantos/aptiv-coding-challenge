package sg.aptiv.booking.model;

/**
 *
 * Represents a Car in the system.
 *
 * A Car can be either booked or available, and is always at a location.
 *
 * @author Nuno Santos
 */
public class Car {

    private int id;

    private Location currentLocation;

    private boolean booked;

    public Car(int id, Location currentLocation) {
        this.id = id;
        this.currentLocation = currentLocation;
        this.booked = false;
    }

    public void setCurrentLocation(Location currentLocation) {
        this.currentLocation = currentLocation;
    }

    public void setBooked(boolean booked) {
        this.booked = booked;
    }

    public boolean isBooked() {
        return booked;
    }

    public Location getCurrentLocation() {
        return currentLocation;
    }

    public int getId() {
        return id;
    }
}
