package sg.aptiv.booking.model;

import java.util.Deque;

/**
 *
 *
 * @author Nuno Santos
 */
public class Journey {

    private int totalTime;

    private int carId;

    private Deque<Location> locations;

    public Journey(int totalTime, int carId, Deque<Location> locations) {
        this.totalTime = totalTime;
        this.carId = carId;
        this.locations = locations;
    }

    public int getTotalTime() {
        return totalTime;
    }

    public int getCarId() {
        return carId;
    }

    public Deque<Location> getLocations() {
        return locations;
    }

    public void setTotalTime(int totalTime) {
        this.totalTime = totalTime;
    }
}
