package sg.aptiv.booking.api;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import sg.aptiv.booking.api.model.BookRequest;
import sg.aptiv.booking.api.model.BookResponse;
import sg.aptiv.booking.service.BookingService;
import sg.aptiv.booking.service.car.CarService;
import sg.aptiv.booking.service.journey.JourneyService;

@RestController
@RequestMapping("/api")
public class APIController {

    @Value("${tick.time.unit}")
    private int timeUnit;

    private CarService carRepository;

    private BookingService bookingService;

    private JourneyService journeyService;

    @Autowired
    public APIController(CarService carRepository, BookingService bookingService, JourneyService journeyManager) {
        this.carRepository = carRepository;
        this.bookingService = bookingService;
        this.journeyService = journeyManager;
    }

    @ApiOperation(value = "Create a booking in the system")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Successfully created a booking in the system", response = BookResponse.class),
            @ApiResponse(code = 202, message = "Request is valid but no cars are available", response = Void.class),
            @ApiResponse(code = 400, message = "The request object is not in the right format", response = Void.class),
    })
    @ResponseStatus(value = HttpStatus.CREATED)
    @PostMapping(value = "/book")
    public ResponseEntity book(@RequestBody BookRequest bookRequest){
        var availableCars = carRepository.getAvailableCars();

        if (availableCars.isEmpty()) {
           return ResponseEntity.ok().build();
        }

        var journey = bookingService.makeBooking(availableCars, bookRequest.getSource(), bookRequest.getDestination());

        carRepository.bookCar(journey.getCarId());
        journeyService.addJourney(journey);

        return new ResponseEntity<>(new BookResponse(journey.getCarId(), journey.getTotalTime()), HttpStatus.CREATED);
    }

    @ApiOperation(value = "Advances the journey in the system by a time unit")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully advanced the journeys")
    })
    @ResponseStatus(value = HttpStatus.OK)
    @PostMapping(value = "/tick")
    public ResponseEntity tick(){
        journeyService.advanceJourneys(timeUnit);

        return ResponseEntity.ok().build();
    }

    @ApiOperation(value = "Resets the state of the system, including journeys and cars.")
    @ApiResponses(value = {
            @ApiResponse(code = 204, message = "Successfully reset the system")
    })
    @ResponseStatus(value = HttpStatus.NO_CONTENT)
    @PutMapping(value = "/reset")
    public ResponseEntity reset(){
        journeyService.clearOnGoingJourneys();

        journeyService.clearFinishedJourneys();

        carRepository.reset();

        return ResponseEntity.noContent().build();
    }
}
