package sg.aptiv.booking.api.model;

public class BookResponse {

    private int car_id;

    private int total_time;

    public BookResponse(int car_id, int total_time) {
        this.car_id = car_id;
        this.total_time = total_time;
    }

    public int getCar_id() {
        return car_id;
    }

    public void setCar_id(int car_id) {
        this.car_id = car_id;
    }

    public int getTotal_time() {
        return total_time;
    }

    public void setTotal_time(int total_time) {
        this.total_time = total_time;
    }
}
