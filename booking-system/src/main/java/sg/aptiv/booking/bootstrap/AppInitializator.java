package sg.aptiv.booking.bootstrap;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import sg.aptiv.booking.model.Car;
import sg.aptiv.booking.model.Location;
import sg.aptiv.booking.service.car.CarService;

import javax.annotation.PostConstruct;

/**
 * Simple init class that bootstraps the system
 * with the Car dataset.
 *
 * @author Nuno Santos
 */
@Component
public class AppInitializator {

    private CarService carRepository;

    @Autowired
    public AppInitializator(CarService carRepository) {
        this.carRepository = carRepository;
    }

    @PostConstruct
    private void init() {
        carRepository.save(new Car(1, new Location(0, 0)));
        carRepository.save(new Car(2, new Location(0, 0)));
        carRepository.save(new Car(3, new Location(0, 0)));
    }
}
