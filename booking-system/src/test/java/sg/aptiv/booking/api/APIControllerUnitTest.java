package sg.aptiv.booking.api;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import sg.aptiv.booking.api.model.BookRequest;
import sg.aptiv.booking.api.model.BookResponse;
import sg.aptiv.booking.model.Car;
import sg.aptiv.booking.model.Journey;
import sg.aptiv.booking.model.Location;
import sg.aptiv.booking.service.BookingService;
import sg.aptiv.booking.service.car.CarService;
import sg.aptiv.booking.service.journey.JourneyService;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@WebMvcTest(APIController.class)
class APIControllerUnitTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private CarService carService;

    @MockBean
    private BookingService bookingService;

    @MockBean
    private JourneyService journeyService;

    @Test
    void bookEndpointShouldReturnExpectedResponseForAvailableCar() throws Exception {
        var carId = 1;
        var source = new Location(0, 0);
        var destination = new Location(1, 1);

        var availableCars = List.of(new Car(carId, source));

        when(carService.getAvailableCars()).thenReturn(availableCars);

        when(bookingService.makeBooking(availableCars, source, destination)).thenReturn(new Journey(1, carId, new LinkedList<>()));

        var expectedResponse = new BookResponse(carId, 1);
        mockMvc.perform(MockMvcRequestBuilders
                .post("/api/book")
                .content(asJsonString(new BookRequest(source, destination)))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated())
                .andExpect(content().json(asJsonString(expectedResponse)));
    }

    @Test
    void bookEndpointShouldReturnEmptyResponseIfNoAvailableCars() throws Exception {

        when(carService.getAvailableCars()).thenReturn(Collections.emptyList());

        mockMvc.perform(MockMvcRequestBuilders
                .post("/api/book")
                .content(asJsonString(new BookRequest(new Location(0, 0), new Location(1, 1))))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().string(""));
    }

    @Test
    void tickEndpointShouldAdvanceJourneys() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders
                .post("/api/tick"))
                .andExpect(status().isOk());

        verify(journeyService, times(1)).advanceJourneys(1);
    }

    @Test
    void resetEndpointShouldResetAllJourneysAndCars() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders
                .put("/api/reset"))
                .andExpect(status().isNoContent());

        verify(journeyService, times(1)).clearFinishedJourneys();
        verify(journeyService, times(1)).clearOnGoingJourneys();
        verify(carService, times(1)).reset();
    }

    private static String asJsonString(final Object obj) {
        try {
            return new ObjectMapper().writeValueAsString(obj);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}