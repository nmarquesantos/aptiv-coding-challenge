package sg.aptiv.booking.api;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import sg.aptiv.booking.api.model.BookRequest;
import sg.aptiv.booking.api.model.BookResponse;
import sg.aptiv.booking.model.Location;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@AutoConfigureMockMvc
@SpringBootTest
class APIControllerIntegrationTest {

    @Autowired
    private MockMvc mockMvc;

    @Test
    void initialBookingForAllCars() throws Exception {
        var firstRequest = new BookRequest(new Location(1, 0), new Location(1, 1));
        executeRequest(firstRequest, new BookResponse(1, 2));

        var secondRequest = new BookRequest(new Location(1, 1), new Location(5, 5));
        executeRequest(secondRequest, new BookResponse(2, 10));

        var thirdRequest = new BookRequest(new Location(-1, 1), new Location(5, 10));
        executeRequest(thirdRequest, new BookResponse(3, 17));
    }

    void executeRequest(BookRequest request, BookResponse response) throws Exception {
        mockMvc.perform(MockMvcRequestBuilders
                .post("/api/book")
                .content(asJsonString(request))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated())
                .andExpect(content().json(asJsonString(response)));
    }

    private static String asJsonString(final Object obj) {
        try {
            return new ObjectMapper().writeValueAsString(obj);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}