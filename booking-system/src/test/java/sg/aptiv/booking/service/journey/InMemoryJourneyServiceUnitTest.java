package sg.aptiv.booking.service.journey;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import sg.aptiv.booking.model.Journey;
import sg.aptiv.booking.model.Location;
import sg.aptiv.booking.service.car.CarService;
import sg.aptiv.booking.service.car.InMemoryCarService;

import java.util.LinkedList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = {InMemoryCarService.class, InMemoryJourneyService.class})
class InMemoryJourneyServiceUnitTest {

    @Autowired
    private JourneyService journeyManager;

    @MockBean
    private CarService carRepository;

    @Test
    void advanceOneJourney() {
        assertNotNull(journeyManager);

        var journeyLocation = new LinkedList<Location>();
        journeyLocation.add(new Location(2, 2));
        journeyLocation.add(new Location(2, 3));

        Journey journey = new Journey(2, 1, journeyLocation);
        journeyManager.addJourney(journey);

        journeyManager.advanceJourneys(1);

        List<Journey> onGoingJourneys = journeyManager.getOnGoingJourneys();
        List<Journey> finishedJourneys = journeyManager.getFinishedJourneys();

        assertTrue(onGoingJourneys.isEmpty());
        assertEquals(1, finishedJourneys.size());

        verify(carRepository, times(1)).freeUpCar(1);
        verify(carRepository, times(1)).updateLocation(1, new Location(2, 3));
    }

}