package sg.aptiv.booking.service.journey.search;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import sg.aptiv.booking.model.Car;
import sg.aptiv.booking.model.Journey;
import sg.aptiv.booking.model.Location;
import sg.aptiv.booking.service.journey.search.astar.AStarAlgorithm;
import sg.aptiv.booking.service.journey.search.astar.Node;
import sg.aptiv.booking.service.journey.search.astar.AStarSearchService;
import sg.aptiv.booking.service.journey.search.SearchService;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = {AStarSearchService.class})
class AStarSearchServiceUnitTest {

    @SpyBean
    private AStarAlgorithm algorithm;

    @Autowired
    private SearchService journeyService;

    private Journey journey;

    private Car car;

    @Test
    void calculateSimpleJourney() {
        var carLocation = new Location(0, 0);
        var source = new Location(1 ,0);
        var destination = new Location(1 ,1);

        var carNode = new Node(0, 0);
        var sourceNode = new Node(1, 0);
        var destinationNode = new Node(1, 1);

        car = new Car(1, carLocation);

        journey = journeyService.searchShortestJourney(car, source, destination);

        verify(algorithm, times(1)).search(carNode, sourceNode);
        verify(algorithm, times(1)).search(sourceNode, destinationNode);

        assertEquals(car.getId(), journey.getCarId());
        assertEquals(2, journey.getTotalTime());
        assertEquals(3, journey.getLocations().size());
    }

    @Test
    void calculateBackAndForthJourney() {
        var carLocation = new Location(0, 0);
        var source = new Location(-1 ,0);
        var destination = new Location(1 ,1);

        var carNode = new Node(0, 0);
        var sourceNode = new Node(-1, 0);
        var destinationNode = new Node(1, 1);

        car = new Car(1, carLocation);

        journey = journeyService.searchShortestJourney(car, source, destination);

        verify(algorithm, times(1)).search(carNode, sourceNode);
        verify(algorithm, times(1)).search(sourceNode, destinationNode);

        assertEquals(car.getId(), journey.getCarId());
        assertEquals(4, journey.getTotalTime());
        assertEquals(5, journey.getLocations().size());
    }

    @Test
    void calculateLongerJourney() {
        var carLocation = new Location(0, 0);
        var source = new Location(-1 ,1);
        var destination = new Location(5 ,10);

        var carNode = new Node(0, 0);
        var sourceNode = new Node(-1, 1);
        var destinationNode = new Node(5, 10);

        car = new Car(1, carLocation);

        journey = journeyService.searchShortestJourney(car, source, destination);

        verify(algorithm, times(1)).search(carNode, sourceNode);
        verify(algorithm, times(1)).search(sourceNode, destinationNode);

        assertEquals(car.getId(), journey.getCarId());
        assertEquals(17, journey.getTotalTime());
        assertEquals(18, journey.getLocations().size());
    }
}