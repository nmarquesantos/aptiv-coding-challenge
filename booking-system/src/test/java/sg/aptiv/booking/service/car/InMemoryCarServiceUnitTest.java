package sg.aptiv.booking.service.car;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import sg.aptiv.booking.model.Car;
import sg.aptiv.booking.model.Location;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = InMemoryCarService.class)
class InMemoryCarServiceUnitTest {

    @Autowired
    private CarService carService;

    @BeforeEach
    void beforeEach() {
        carService.reset();
        carService.save(new Car(1, new Location(0, 0)));
        carService.save(new Car(2, new Location(0, 0)));
        carService.save(new Car(3, new Location(0, 0)));
    }

    @Test
    void getCarById() {
        var car = carService.get(1);
        assertNotNull(car);
    }

    @Test
    void getAvailableCars() {
        var availableCars = carService.getAvailableCars();
        assertEquals(3, availableCars.size());
    }

    @Test
    void getBookedCars() {
        var availableCars = carService.getAvailableCars();
        var bookedCars = carService.getBookedCars();

        assertEquals(3, availableCars.size());
        assertEquals(0, bookedCars.size());

        carService.bookCar(1);

        availableCars = carService.getAvailableCars();
        bookedCars = carService.getBookedCars();
        assertEquals(2, availableCars.size());
        assertEquals(1, bookedCars.size());
    }

    @Test
    void freeUpCar() {
        var availableCars = carService.getAvailableCars();
        var bookedCars = carService.getBookedCars();

        assertEquals(3, availableCars.size());
        assertEquals(0, bookedCars.size());

        carService.bookCar(1);
        availableCars = carService.getAvailableCars();
        bookedCars = carService.getBookedCars();
        assertEquals(2, availableCars.size());
        assertEquals(1, bookedCars.size());

        carService.freeUpCar(1);
        availableCars = carService.getAvailableCars();
        bookedCars = carService.getBookedCars();
        assertEquals(3, availableCars.size());
        assertEquals(0, bookedCars.size());
    }
}