package sg.aptiv.booking.service;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import sg.aptiv.booking.model.Car;
import sg.aptiv.booking.model.Location;
import sg.aptiv.booking.service.journey.search.astar.AStarSearchService;
import sg.aptiv.booking.service.journey.search.SearchService;
import sg.aptiv.booking.service.journey.search.astar.AStarAlgorithm;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = {AStarSearchService.class, SimpleBookingService.class, AStarAlgorithm.class})
class SimpleBookingServiceUnitTest {

    @SpyBean
    private SearchService journeyService;

    @Autowired
    private BookingService bookingService;

    private List<Car> availableCars;

    private Location source;

    private Location destination;

    @BeforeEach
    void beforeEach() {
        availableCars = new LinkedList<>();
    }

    @Test
    void makeSingleCarBooking() {
        var car = new Car(1, new Location(0 ,0));
        source = new Location(1, 0);
        destination = new Location(1, 1);
        availableCars.add(car);

        var journey = bookingService.makeBooking(availableCars, source, destination);

        verify(journeyService, times(1)).searchShortestJourney(car, source, destination);

        assertEquals(car.getId(), journey.getCarId());
    }

    @Test
    void makeBookingWithTwoCarsSameLocation() {
        source = new Location(1, 0);
        destination = new Location(1, 1);

        var firstCar = new Car(1, new Location(0, 0));
        var secondCar = new Car(2, new Location(0, 0));

        availableCars.add(firstCar);
        availableCars.add(secondCar);

        var journey = bookingService.makeBooking(availableCars, source, destination);

        verify(journeyService, times(1)).searchShortestJourney(firstCar, source, destination);
        verify(journeyService, times(1)).searchShortestJourney(secondCar, source, destination);

        assertEquals(firstCar.getId(), journey.getCarId());
    }

    @Test
    void makeBookingWithTwoCarsDifferentLocation() {
        source = new Location(1, 0);
        destination = new Location(1, 1);

        var firstCar = new Car(1, new Location(0, 0));
        var secondCar = new Car(2, new Location(1, 0));

        availableCars.add(firstCar);
        availableCars.add(secondCar);

        var journey = bookingService.makeBooking(availableCars, source, destination);

        verify(journeyService, times(1)).searchShortestJourney(firstCar, source, destination);
        verify(journeyService, times(1)).searchShortestJourney(secondCar, source, destination);

        assertEquals(secondCar.getId(), journey.getCarId());
    }


    @Test
    void makeBookingShouldThrowExceptionIfAvailableCarsIsNull() {
        assertThrows(IllegalArgumentException.class, () -> bookingService.makeBooking(null, new Location(0 ,0), new Location(0 ,0)));
    }

    @Test
    void makeBookingShouldThrowExceptionIfNoAvailableCars() {
        assertThrows(IllegalArgumentException.class, () -> bookingService.makeBooking(Collections.emptyList(), new Location(0 ,0), new Location(0 ,0)));
    }

    @Test
    void makeBookingShouldThrowExceptionIfSourceIsNull() {
        assertThrows(IllegalArgumentException.class, () -> bookingService.makeBooking(List.of(new Car(1, new Location(0,  0))), null , new Location(0 ,0)));
    }

    @Test
    void makeBookingShouldThrowExceptionIfDestinationIsNull() {
        assertThrows(IllegalArgumentException.class, () -> bookingService.makeBooking(List.of(new Car(1, new Location(0,  0))) , new Location(0 ,0), null));
    }


}