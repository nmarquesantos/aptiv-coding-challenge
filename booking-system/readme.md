# Instructions

This project has been developed as  a Web App.

Gradle has been chosen as the build tool while Spring Boot was chosen as the Web Framework.

The solution has been designed as per the requirements specified in problem.md

The application runs on the default port 8080.

## Prerequisites
The only prerequisite to build and run this application is OpenJDK 11.

Depending on the OS of choice, install your desired distribution from here: https://www.oracle.com/technetwork/java/javase/downloads/jdk11-downloads-5066655.html

Note: for the first time running the gradle command, it can take a little bit as it will download the gradle binaries on the first run.
## Run Tests

From the root of the project, execute: 
```./gradlew clean test```

## Run app
From the root of the project, execute:
```./gradlew clean bootRun```

##Documentation

Swagger documentation for the API can be accessed here, after the Application is running: http://localhost:8080/swagger-ui.html

## Assumptions/Limitations

Due to performance limitations, I have not met the requirements of the grid size. 

They can, however, be changed in AStarAlgorithm class. 

Due to time constraints, I have not extensively added validation at the API level.

